'use strict';

define('admin/plugins/prometheus', ['settings'], function(Settings) {
    let prometheus = {};

    prometheus.load = function() {
        Settings.load('nodebb-plugin-prometheus', $('#nodebb-plugin-prometheus-settings'), function (err, settings) {
            if(err) {
                return;
            }

            let url = config.relative_path + settings.metricsPath;
            if(settings.metricsAccessToken) {
                url += '?token=' + settings.metricsAccessToken;
            }

            $('#metricsUrl').attr('href', url).text(url);
        });
    };

    prometheus.init = function() {
        prometheus.load();

        $('#nodebb-plugin-prometheus-settings-save').on('click', function() {
            Settings.save('nodebb-plugin-prometheus', $('#nodebb-plugin-prometheus-settings'), function() {
                app.alert({
                    type: 'success',
                    alert_id: 'nodebb-plugin-prometheus-saved',
                    title: '[[nodebb-plugin-prometheus:settings-saved-title]]',
                    message: '[[nodebb-plugin-prometheus:settings-saved-message]]',
                    timeout: 5000,
                    clickfn: function() {
                        socket.emit('admin.reload');
                    }
                });
            });
        });

        $('#generateMetricsAccessToken').on('click', function(e) {
            function uuidv4() {
                return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
                    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
                )
            }

            $('#metricsAccessToken').val(uuidv4());
            e.preventDefault();
        });

        $(window).on('action:reconnected', function () {
            if(window.location.pathname.startsWith(config.relative_path + '/admin/plugins/prometheus')) {
                prometheus.load();
            }
        });
    };

    return prometheus;
});