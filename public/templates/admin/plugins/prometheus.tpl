<div class="row">
    <div class="col">
        <form role="form" class="form-horizontal nodebb-plugin-prometheus-settings" id="nodebb-plugin-prometheus-settings">
            <div class="form-group">
                <label for="metricsAccessToken">[[nodebb-plugin-prometheus:metrics-access-token-label]]</label>
                <div class="col-sm-11" style="padding-left:0;">
                    <input class="form-control" type="text" id="metricsAccessToken" name="metricsAccessToken">
                </div>
                <div class="col-sm-1">
                    <button class="btn btn-default" id="generateMetricsAccessToken">[[nodebb-plugin-prometheus:generate-metrics-access-token]]</button>
                </div>
            </div>
            <br>
            <div class="form-group">
                <label for="metricsPath">[[nodebb-plugin-prometheus:metrics-path-label]]</label>
                <input class="form-control" type="text" id="metricsPath" name="metricsPath" placeholder="/metrics">
            </div>
            <br>
            <div class="form-group">
                <label for="nodeUpdateInterval">[[nodebb-plugin-prometheus:node-update-interval-label]]</label>
                <input class="form-control" type="number" id="nodeUpdateInterval" name="nodeUpdateInterval" placeholder="10">
            </div>
        </form>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col">
        <p>
            [[nodebb-plugin-prometheus:metrics-path-prefix]] <a id="metricsUrl" href=""></a>
        </p>
    </div>
</div>

<button id="nodebb-plugin-prometheus-settings-save" class="floating-button mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored">
    <i class="material-icons">save</i>
</button>