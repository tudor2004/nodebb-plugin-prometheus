'use strict';

let plugin = {};

let async = require('async');
let promClient = require('prom-client');
let uuidv4 = require('uuid/v4');

let db = require.main.require('./src/database');
let meta = require.main.require('./src/meta');
let plugins = require.main.require('./src/plugins');
let winston = require.main.require('winston');


class FieldGauge {
    constructor(name, help, objectKey, objectField) {
        this.gauge = new promClient.Gauge({
            name: name,
            help: help
        });

        this.key = objectKey;
        this.field = objectField;

        this.update();
    }

    update(callback) {
        callback = callback || function(){};
        let self = this;
        db.getObjectField(this.key, this.field, function (err, value) {
            if(!err) {
                self.gauge.set(parseFloat(value));
            } else {
                winston.warn("[plugin/prometheus] Failed to collect data for " + self.gauge.name + ": " + err);
            }

            callback();
        });
    }
}


class TimeDeltaGauge {
    constructor(name, help, setKey, timeDelta) {
        this.gauge = new promClient.Gauge({
            name: name,
            help: help
        });

        this.key = setKey;
        this.timeDelta = timeDelta || '-inf';

        this.update();
    }

    update(callback) {
        callback = callback || function(){};
        let self = this;
        let now = new Date().getTime();
        db.sortedSetCount(this.key, now - this.timeDelta, '+inf', function (err, value) {
            if(!err) {
                self.gauge.set(value);
            } else {
                winston.warn("[plugin/prometheus] Failed to collect data for " + self.gauge.name + ": " + err);
            }

            callback();
        });
    }
}


class SortedSetGauge {
    constructor(name, help, setKey, min, max) {
        this.gauge = new promClient.Gauge({
            name: name,
            help: help
        });

        this.key = setKey;
        this.min = min || '-inf';
        this.max = max || '+inf';

        this.update();
    }

    update(callback) {
        callback = callback || function(){};
        let self = this;
        db.sortedSetCount(this.key, this.min, this.max, function (err, value) {
            if(!err) {
                self.gauge.set(value);
            } else {
                winston.warn("[plugin/prometheus] Failed to collect data for " + self.gauge.name + ": " + err);
            }

            callback();
        });
    }
}


class CustomGauge {
    constructor(name, help, updateFn) {
        this.gauge = new promClient.Gauge({
            name: name,
            help: help
        });

        this.updateFn = updateFn || function (_, callback) { callback(); };

        this.update();
    }

    update(callback) {
        callback = callback || function(){};
        this.updateFn(this.gauge, callback);
    }
}


class PluginGauge {
    constructor() {
        this.gauge = new promClient.Gauge({
            name: 'nodebb_plugins_active',
            help: 'If the plugin is active 1; otherwise 0. Has a label id with the name of the plugin',
            labelNames: ['id']
        });

        this.update();
    }

    update(callback) {
        callback = callback || function(){};
        let self = this;
        plugins.showInstalled(function (err, plugins) {
            self.gauge.reset();
            for(let plugin of plugins) {
                self.setOne(plugin.id, plugin.active);
            }

            callback();
        });
    }

    setOne(id, isActive) {
        this.gauge.set({id: id}, isActive ? 1 : 0);
    }
}


plugin.init = function(params, callback) {
    params.router.get('/admin/plugins/prometheus', params.middleware.admin.buildHeader, renderAdmin);
    params.router.get('/api/admin/plugins/prometheus', renderAdmin);

    meta.settings.get('nodebb-plugin-prometheus', function(err, settings) {
        if(err) {
            return callback(err);
        }

        function initSettingProperty(property, defaultValue) {
            if(!settings.hasOwnProperty(property)) {
                settings[property] = defaultValue;
            }
        }

        initSettingProperty('metricsAccessToken', uuidv4());
        initSettingProperty('metricsPath', '/metrics');
        initSettingProperty('nodeUpdateInterval', 10);

        params.router.get(settings.metricsPath, renderMetrics);

        settings.nodeUpdateInterval = parseInt(settings.nodeUpdateInterval, 10);
        if(settings.nodeUpdateInterval) {
            promClient.collectDefaultMetrics({ timeout: settings.nodeUpdateInterval * 1000 });
        }

        plugin.settings = settings;
        meta.settings.set('nodebb-plugin-prometheus', settings);
    });

    plugins.fireHook('action:nodebb-plugin-prometheus.init', { prometheus: promClient }, callback);
};

plugin.addAdminNavigation = function(header, callback) {
    header.plugins.push({
        route: '/plugins/prometheus',
        icon: 'fa-chart-bar',
        name: 'Prometheus'
    });

    callback(null, header);
};

plugin.analyticsCounterIncrement = function (data) {
    data.keys.forEach(function (key) {
        const counter = plugin.analyticsCounter.get(key);
        if(counter !== undefined) {
            counter.inc(1);
        }
    });
};

const timeDeltas = {
    fiveMinutes: 5 * 60 * 1000,
    oneHour: 60 * 60 * 1000,
    oneDay: 24 * 60 * 60 * 1000,
    thirtyDays: 30 * 24 * 60 * 60 * 1000
};

plugin.analyticsCounter = new Map();
plugin.analyticsCounter.set('flags', new promClient.Counter({
    name: 'nodebb_flags_total',
    help: 'Total issued flags since last restart'
}));

plugin.analyticsCounter.set('errors:404', new promClient.Counter({
    name: 'nodebb_error_404_total',
    help: 'Total HTTP 404 errors since last restart'
}));

plugin.analyticsCounter.set('errors:503', new promClient.Counter({
    name: 'nodebb_error_503_total',
    help: 'Total HTTP 503 errors since last restart'
}));

plugin.analyticsCounter.set('blacklist', new promClient.Counter({
    name: 'nodebb_blacklisted_total',
    help: 'Total request blocked via the blacklist since last restart'
}));

plugin.metrics = [
    new FieldGauge('nodebb_users_total', 'Total registered users', 'global', 'userCount'),
    new FieldGauge('nodebb_posts_total', 'Total posts', 'global', 'postCount'),
    new FieldGauge('nodebb_topics_total', 'Total topics', 'global', 'topicCount'),
    new TimeDeltaGauge('nodebb_unique_users_5m', 'Unique users over the last five minutes', 'ip:recent', timeDeltas.fiveMinutes),
    new TimeDeltaGauge('nodebb_unique_users_60m', 'Unique users over the last 60 minutes', 'ip:recent', timeDeltas.oneHour),
    new TimeDeltaGauge('nodebb_unique_users_24h', 'Unique users over the last 24 hours', 'ip:recent', timeDeltas.oneDay),
    new TimeDeltaGauge('nodebb_unique_users_30d', 'Unique users over the 30 days', 'ip:recent', timeDeltas.thirtyDays),
    new TimeDeltaGauge('nodebb_online_users_5m', 'Users online over the last five minutes', 'users:online', timeDeltas.fiveMinutes),
    new TimeDeltaGauge('nodebb_online_users_60m', 'Users online over the last 60 minutes', 'users:online', timeDeltas.oneHour),
    new TimeDeltaGauge('nodebb_online_users_24h', 'Users online over the last 24 hours', 'users:online', timeDeltas.oneDay),
    new TimeDeltaGauge('nodebb_online_users_30d', 'Users online over the last 30 days', 'users:online', timeDeltas.thirtyDays),
    new SortedSetGauge('nodebb_not_validated_users_total', 'Total users with the e-mail address not validated', 'users:notvalidated'),
    new SortedSetGauge('nodebb_banned_users_total', 'Total banned users', 'users:banned'),
    new SortedSetGauge('nodebb_registration_queue_total', 'Total users waiting in the registration queue', 'registration:queue'),
    new CustomGauge('nodebb_maintenance_active', 'If maintenance mode is on 1; otherwise 0', (g, c) => {
        g.set((parseInt(meta.config.maintenanceMode, 10) !== 1) ? 0 : 1);
        c();
    }),
    new CustomGauge('nodebb_online_guests', 'Guests that are online and connected at the moment', (g, c) => {
        require.main.require('./src/socket.io/admin/rooms').getTotalGuestCount(function (err, count) {
            if(err) {
                winston.warn("[plugin/prometheus] Failed to collect data for online guest count: " + err);
            } else {
                g.set(count);
            }

            c();
        });
    }),
    new CustomGauge('nodebb_eventloop_lag_seconds', 'Lag of event loop in seconds measured by NodeBB', (g, c) => {
        g.set(module.parent.require('toobusy-js').lag() / 1000.0);
        c();
    }),
    new CustomGauge('nodebb_eventloop_maxlag_seconds', 'Maximum lag of event loop in seconds allowed by NodeBB before it will start to issue 503 errors', (g, c) => {
        g.set(module.parent.require('toobusy-js').maxLag() / 1000.0);
        c();
    })
];

let pageViewCounter = new promClient.Counter({
    name: 'nodebb_page_view_total',
    help: 'Total page views since the last restart',
    labelNames: ['viewer']
});

plugin.pageView = function (data) {
    let type;
    if (data.req.uid > 0) {
        type = 'user';
    } else if (data.req.uid < 0) {
        type = 'bot';
    } else {
        type = 'guest';
    }

    pageViewCounter.inc({viewer: type}, 1);
};

let pluginGauge = new PluginGauge();

plugin.updateMetrics = function (params, callback) {
    async.each(plugin.metrics, function(item, callback) {
        item.update(callback);
    }, callback);
};

plugin.pluginActivate = function(data) {
    pluginGauge.setOne(data.id, true);
};

plugin.pluginDeactivate = function(data) {
    pluginGauge.setOne(data.id, false);
};

plugin.pluginInstall = function(data) {
    pluginGauge.setOne(data.id, data.active);
};

plugin.pluginUninstall = function() {
    pluginGauge.update();
};

function renderAdmin(req, res) {
    res.render('admin/plugins/prometheus', {});
}

function renderMetrics(req, res) {
    if(plugin.settings.metricsAccessToken && req.query.token !== plugin.settings.metricsAccessToken) {
        res.sendStatus(404);
        return;
    }

    plugins.fireHook('static:nodebb-plugin-prometheus.metrics', {}, function (err) {
        if(err) {
            winston.error('[plugin/prometheus] Failed scraping the metrics! ' + err.message);
            res.sendStatus(500);
            return;
        }

        res.set('Cache-Control', 'no-cache');
        res.set('Content-Type', promClient.register.contentType);
        res.send(promClient.register.metrics({timestamps: false}));
    });
}


module.exports = plugin;
