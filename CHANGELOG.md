# Changelog

## v0.4.0 - 2018-12-28

* Fixed warnings of require in NodeBB 1.11

## v0.3.0 - 2018-10-27

* Using `action:analytics.increment` for analytics counter (better performance and accuracy)
* Added label `viewer` to page view that can have the values `user`, `guest`, or `bot`
* Added new metric `nodebb_online_guests`
* Added new metric `nodebb_eventloop_lag_seconds`. This is measured by NodeBB which has a smoothing factored compared to what is measured by NodeJS and provided via `nodejs_eventloop_lag_seconds`
* Added new metric `nodebb_eventloop_maxlag_seconds`. This is the maximum allowed lag for NodeBB. If the lag is getting closer to this value NodeBB will start to issue 503 error codes. The chance a user is getting a 503 is calculate by this formula: `(lag - maxLage) / maxLag`.

## v0.2.1 - 2018-07-02

* Fixed compatibility with Redis
* Fixed a typo in the code

## v0.2.0 - 2018-06-11

* Changed the hook `action:nodebb-plugin-prometheus.metrics` to a static hook `static:nodebb-plugin-prometheus.metrics`. It will be called when a scape request is received
* Removed metric `nodebb_flagged_users_total` since it showed a wrong value
* Added metric `nodebb_flags_total`
* Fixed metric `nodebb_error_404_total` showing now the right value
* Changed metric `nodebb_error_404_total` from gauge to counter
* Fixed metric `nodebb_error_503_total` showing now the right value
* Changed metric `nodebb_error_503_total` from gauge to counter
* Fixed metric `nodebb_blacklisted_total` showing now the right value
* Changed metric `nodebb_blacklisted_total` from gauge to counter
* There is still a slight issue with `nodebb_blacklisted_total`, `nodebb_error_503_total`, `nodebb_error_404_total`, and `nodebb_flags_total` but it should not cause any major issue. Problem is documented in the readme. Fix for this might come later.  

## v0.1.2 - 2018-06-10

* Removing timestamps from all metrics  
  https://github.com/siimon/prom-client/issues/177
  https://www.youtube.com/watch?v=GcTzd2CLH7I

## v0.1.1 - 2018-06-10

* Fixed page view metric (there is now only one metric and it is a counter)

## v0.1.0 - 2018-06-10

* Initial release