# nodebb-plugin-prometheus

![nodebb compatibility](https://packages.nodebb.org/api/v1/plugins/nodebb-plugin-prometheus/compatibility.png)

Discuss it or give feedback in the [NodeBB community](https://community.nodebb.org/topic/12493/nodebb-plugin-prometheus-prometheus-monitoring-for-nodebb) or create an issue in the [repository](https://gitlab.com/cppnet/nodebb/nodebb-plugin-prometheus/issues).

Provides a Prometheus exporter endpoint to your NodeBB and exposes metrics. You can secure the endpoint either via your reverse proxy or an access token validated by the plugin. By default the endpoint will be provided at `/metrics`. Find all the settings in the plugin's ACP page.

The following metrics are provided for NodeBB:

|Key|Type|Help|
|---|----|-----|
|nodebb_users_total|gauge|Total registered users|
|nodebb_posts_total|gauge|Total posts|
|nodebb_topics_total|gauge|Total topics|
|nodebb_unique_users_5m|gauge|Unique users over the last five minutes|
|nodebb_unique_users_60m|gauge|Unique users over the last 60 minutes|
|nodebb_unique_users_24h|gauge|Unique users over the last 24 hours|
|nodebb_unique_users_30d|gauge|Unique users over the 30 days|
|nodebb_page_view_total|counter|Total page views since the last restart. There is a label `viewer` that can have the value `user`, `guest`, or `bot`|
|nodebb_online_users_5m|gauge|Users online over the last five minutes|
|nodebb_online_users_60m|gauge|Users online over the last 60 minutes|
|nodebb_online_users_24h|gauge|Users online over the last 24 hours|
|nodebb_online_users_30d|gauge|Users online over the last 30 days|
|nodebb_not_validated_users_total|gauge|Total users with the e-mail address not validated|
|nodebb_banned_users_total|gauge|Total banned users|
|nodebb_registration_queue_total|gauge|Total users waiting in the registration queue|
|nodebb_flags_total|counter|Total issued flags since last restart|
|nodebb_error_404_total|counter|Total HTTP 404 errors since last restart|
|nodebb_error_503_total|counter|Total HTTP 503 errors since last restart|
|nodebb_blacklisted_total|counter|Total request blocked via the blacklist since last restart|
|nodebb_maintenance_active|gauge|If maintenance mode is on 1; otherwise 0|
|nodebb_online_guests|gauge|Guests that are currently online/connected|
|nodebb_eventloop_lag_seconds|Gauge|Lag of event loop in seconds measured by NodeBB|
|nodebb_eventloop_maxlag_seconds|Gauge|Maximum lag of event loop in seconds allowed by NodeBB before it will start to issue 503 errors|
|nodebb_plugins_active|gauge|If the plugin is active 1; otherwise 0. Has a label id with the name of the plugin|

The counters `nodebb_flags_total`, `nodebb_error_404_total`, `nodebb_error_503_total`, `nodebb_blacklisted_total` currently reset to the value measured in the current hour. This is important to keep in mind if you want to compute the total over one or more restarts, since the counters didn't restart from 0. This will hopefully be fixed in a later version. It might need a patch for NodeBB to make it possible.

In addition default metrics about NodeJS and the process are provided by the prometheus [client library](https://github.com/siimon/prom-client).

|Key|Type|Help|
|---|----|-----|
|process_cpu_user_seconds_total|counter|Total user CPU time spent in seconds.|
|process_cpu_system_seconds_total|counter|Total system CPU time spent in seconds.|
|process_cpu_seconds_total|counter|Total user and system CPU time spent in seconds.|
|process_start_time_seconds|gauge|Start time of the process since unix epoch in seconds.|
|process_resident_memory_bytes|gauge|Resident memory size in bytes.|
|process_virtual_memory_bytes|gauge|Virtual memory size in bytes.|
|process_heap_bytes|gauge|Process heap size in bytes.|
|process_open_fds|gauge|Number of open file descriptors.|
|process_max_fds|gauge|Maximum number of open file descriptors.|
|nodejs_eventloop_lag_seconds|gauge|Lag of event loop in seconds.|
|nodejs_active_handles_total|gauge|Number of active handles.|
|nodejs_active_requests_total|gauge|Number of active requests.|
|nodejs_heap_size_total_bytes|gauge|Process heap size from node.js in bytes.|
|nodejs_heap_size_used_bytes|gauge|Process heap size used from node.js in bytes.|
|nodejs_external_memory_bytes|gauge|Nodejs external memory size in bytes.|
|nodejs_heap_space_size_total_bytes|gauge|Process heap space size total from node.js in bytes.|
|nodejs_heap_space_size_used_bytes|gauge|Process heap space size used from node.js in bytes.|
|nodejs_heap_space_size_available_bytes|gauge|Process heap space size available from node.js in bytes.|
|nodejs_version_info|gauge|Node.js version info.|

If you're developing a plugin and want to add other metrics you can listen for the hook `action:nodebb-plugin-prometheus.init`. It will pass in the Prometheus client in the first parameter: `params.prometheus`. Make sure to prefix your metrics with the name of your plugin replacing dashes with underscores. Listen to the hook `static:nodebb-plugin-prometheus.metrics` to update your metrics. It will be fired when a scrape request is made. Further documentation on how to use the Prometheus client can be found at it's [repository](https://github.com/siimon/prom-client).

```javascript
plugin.initPrometheus = function(params) {
  let gauge = new params.prometheus.Gauge({
    name: 'nodebb_plugin_foo_bar_value_total',
    help: 'The total value of my foobar plugin'
  });
  
  // ...
}
```